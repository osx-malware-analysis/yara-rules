/*
* author: afeltin
* format ref: https://opensource.apple.com/source/Security/Security-57336.1.9/codesign_wrapper/codesign.c.auto.html
* description: identify Mach-O executable binaries using "fake" signature
*/
private rule signed_macho
{
  strings:
    $s0 = {1D 00 00 00 10 00 00 00} // LC_CODE_SIGNATURE
  condition:
    uint32(0x00) == 0xfeedfacf and // MH_MAGIC_64
    uint32(0x0C) == 0x02 and // MACH_EXECUTE
    $s0 in (32..uint32(0x14)+32)
}

rule macho_sig_anomaly {
  strings:
    $s0 = {FA DE 0C C0 } // CSMAGIC_EMBEDDED_SIG
    $s1 = {FA DE 0C 02 [4] 00 02 00 01} // CSMAGIC_CODEDIRECTORY + ver 0x20001 (legacy)
    $s2 = {FA DE 0C 02 [4] 00 02 04 00} // CSMAGIC_CODEDIRECTORY + ver 0x20400 (ldid)
  condition:
    signed_macho and $s0 and ($s1 or $s2)
}
/*
* author: afeltin
* format ref: https://opensource.apple.com/source/Security/Security-57336.1.9/codesign_wrapper/codesign.c.auto.html
* description: identify Mach-O executable binaries using ad-hoc signing
*/
rule macho_sig_adhoc {
  strings:
    $s0 = {FA DE 0C C0 } // CSMAGIC_EMBEDDED_SIG
    $s1 = {FA DE 0C 02 [8] 00 00 00 02} // CSiMAGIC_CODEDIRECTORY + Ad-Hoc flag (0x2)
  condition:
    signed_macho and ($s0 and $s1)
}
