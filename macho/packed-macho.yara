/*
* author: afeltin
* format ref: https://opensource.apple.com/source/cctools/cctools-921/include/mach-o/loader.h.auto.html
* description: identify packed Mach-O executable binaries
*/
// import "macho"
rule packed_macho
{
  strings:
    $s0 = {0E 00 00 00 20 00 00 00} // LC_LOAD_DYLINKER
  condition:
    uint32(0x00) == 0xfeedfacf and // MH_MAGIC_64
    uint32(0x0C) == 0x02 and // MACH_EXECUTE
    uint32(0x70) == 0x45545F5F and
    uint32(0x74) == 0x00005458 and
    uint32(0xA8) == 0x01 and not
    // macho.segments[1].segname == "__TEXT" and macho.segments[1].nsects == 1 and not
      $s0 in (32..uint32(0x14)+32)
}
